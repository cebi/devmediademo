﻿using System.Linq;
using System.Security.Claims;
using System.Web.Http;

namespace DevMediaApi.Controllers
{
    public class FreeStuffController : ApiController
    {
        public IHttpActionResult Get()
        {
            var principal = User as ClaimsPrincipal;

            if (principal == null)
            {
                return BadRequest();
            }

            var myClaim = principal
                .Claims
                .SingleOrDefault(x => x.Type == "userId");

            if (myClaim == null)
            {
                return InternalServerError();
            }

            var retorno = new
            {
                userId = myClaim.Value
            };

            return Ok(retorno);
        }
    }
}
