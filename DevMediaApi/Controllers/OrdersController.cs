﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using DevMediaApi.Models;

namespace DevMediaApi.Controllers
{
    [Authorize]
    [RoutePrefix("Orders")]
    public class OrdersController : ApiController
    {
        public IHttpActionResult GetOrders()
        {
            var lista = GetMyOrders();

            return Ok(lista);
        }

        [Route("MyOrders")]
        [HttpGet]
        public IHttpActionResult GetOrdersByUser()
        {
            int myUserId = ObterUserId();

            var lista = GetMyOrders()
                .Where(x => x.UserId == myUserId);

            return Ok(lista);
        }

        private int ObterUserId()
        {
            var principal = User as ClaimsPrincipal;

            if (principal == null)
            {
                return 0;
            }

            var myClaim = principal
                .Claims
                .SingleOrDefault(x => x.Type == "userId");

            if (myClaim == null)
            {
                return 0;
            }

            return int.Parse(myClaim.Value);
        }

        private List<Order> GetMyOrders()
        {
            var list = new List<Order>
            {
                new Order {OrderId = 1, Number = 111,  UserId = 1, CustomerName = "ABCDEF", Ammout = 123.56M},
                new Order {OrderId = 2, Number = 222,  UserId = 2, CustomerName = "ZUADES", Ammout = 456.78M},
                new Order {OrderId = 3, Number = 333,  UserId = 2, CustomerName = "KUJGTD", Ammout = 789.00M},
                new Order {OrderId = 4, Number = 444,  UserId = 3, CustomerName = "ABCDEF", Ammout = 123.56M},
                new Order {OrderId = 5, Number = 555,  UserId = 3, CustomerName = "ZUADES", Ammout = 456.78M},
                new Order {OrderId = 6, Number = 666,  UserId = 3, CustomerName = "KUJGTD", Ammout = 789.00M}
            };

            return list;
        }
    }
}
