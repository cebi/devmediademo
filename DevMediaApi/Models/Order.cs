namespace DevMediaApi.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public int Number { get; set; }
        public int UserId { get; set; }
        public string CustomerName { get; set; }
        public decimal Ammout { get; set; }
    }
}